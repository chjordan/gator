require "set"
require "fileutils"
require "open3"

class Float
    def close_to?(n, tol: 0.1)
        (self - n).abs < tol
    end
end

class String
    # https://stackoverflow.com/questions/1489183/colorized-ruby-output
    def colorize(color_code)
        "\e[#{color_code}m#{self}\e[0m"
    end

    def red
        colorize(31)
    end

    def green
        colorize(32)
    end

    def yellow
        colorize(33)
    end

    def blue
        colorize(34)
    end

    def pink
        colorize(35)
    end
end

def get_queue(machine:, partition:, user: ENV["USER"])
    queue = `squeue -M #{machine} -p #{partition} -u #{user} 2>&1`
    # Sometimes the queue system needs a little time to think.
    while queue =~ /error/
        STDERR.puts "Slurm error, waiting 10s"
        STDERR.puts "Error: #{queue}"
        sleep 10
        queue = `squeue -M #{machine} -p #{partition} -u #{user} 2>&1`
    end

    jobs_in_queue = queue.split("\n").map { |l| l.split[0].to_i if l =~ /^\d/ }.compact
    len_queue = jobs_in_queue.length
    [queue, jobs_in_queue, len_queue]
end

def asvo_queue
    queue, status = Open3.capture2e("giant-squid -ln")

    # If giant-squid doesn't give an exit code 0, then fail here.
    abort("giant-squid failed! Output:\n#{queue}") \
      unless status.exitstatus == 0

    obsids = { "conversion": {}, "download": {} }
    queue.scan(/^(\S+)\s*: jobID: (\d+) Type: (\S+) Status: (.*?)( Size: .*)?$/).each do |r|
        o = if r[2] == "Visibilities"
                obsids[:download]
            else
                obsids[:conversion]
            end
        o[r[0]] = [r[1], r[3]]
    end
    obsids
end

def submit_asvo_download(obsid:)
    asvo_stdout = `giant-squid submit-vis #{obsid}`
    asvo_stdout.match(/Submitted \d+ as ASVO job ID (\d+)/)[1].to_i
end

def submit_asvo_conversion(obsid:, timeres: 4, freqres: 40, edgewidth: 80,
                           conversion: "ms", allowmissing: true, flagdcchannels: true,
                           options_string: nil)
    # If specified, options_string *must* start with "job_type=c," and be formatted like:
    # job_type=c, timeres=4, freqres=40, edgewidth=80, conversion=ms, allowmissing=true
    command = if options_string
                  "giant-squid submit-conv #{obsid} -p #{options_string}"
              else
                  # The following uses "process substitution" to alleviate the need
                  # for creating a CSV file.
                  "giant-squid submit-conv #{obsid} -p \"job_type=c, " \
                  "timeres=#{timeres}, freqres=#{freqres}, edgewidth=#{edgewidth}, " \
                  "conversion=#{conversion}, allowmissing=#{allowmissing}, " \
                  "flagdcchannels=#{flagdcchannels}\""
              end
    asvo_stdout = `#{command}`

    if asvo_stdout.match?(/Submitted job/)
        asvo_stdout.match(/Submitted job: (\d+)/)[1].to_i
    elsif asvo_stdout.match?(/already exists/)
        asvo_stdout.match(/Job: (\d+) already exists/)[1].to_i
    end
end

def delete_gpubox_files(path:)
    FileUtils.rm_rf(Dir.glob("#{path}/*gpubox*.fits") + Dir.glob("#{path}/.gator_download_complete"))
end

def sbatch(command, low_priority: false)
    output = if low_priority
                 `sbatch --nice=100000 #{command} 2>&1`
             else
                 `sbatch #{command} 2>&1`
             end

    # Sometimes the queue system needs a little time to think.
    while output =~ /error/
        STDERR.puts "Slurm error, waiting 10s"
        STDERR.puts "Error: #{output}"
        sleep 10
        output = if low_priority
                     `sbatch --nice=100000 #{command} 2>&1`
                 else
                     `sbatch #{command} 2>&1`
                 end
    end
    output
end

def mins2hms(mins)
    # The following code uses seconds, so convert from minutes.
    t = (mins * 60).to_i
    "%02d:%02d:%02d" % [t / 86400 * 24 + t / 3600 % 24, t / 60 % 60, t % 60]
end

def write(file:, contents:)
    begin
        File.open(file, "w") { |f| f.puts contents }
    rescue
        STDERR.puts "Could not write to #{file} !"
        exit 1
    end
end

def obtain_obsids(argv)
    # This function parses inputs, determining if they're actually obsids or
    # files containing obsids.
    def check_and_push(obsids, obsid)
        return if obsid.strip.empty? or obsid.to_i.zero?
        obsids.push(obsid.to_i)
    end

    obsids = []
    argv.each do |o|
        # Check if this argument is a file - if so, assume it contains obsids.
        if File.file?(o)
            File.readlines(o).each do |o2|
                check_and_push(obsids, o2)
            end
        # Otherwise, assume the argument is an obsid and add it.
        else
            check_and_push(obsids, o)
        end
    end
    obsids
end

def read_fits_key(fits:, key:)
    m = File.open(fits, "r") { |f| f.read(10_000).match(/#{key}\s*=\s*(\S+)/) }
    return m[1] if m
    nil
end

def alter_config(text:, key:, value:)
    # Comments come as two forward slashes (i.e. //)
    # before a key value.
    results = text.scan(/(..)?(#{key}.*\n)/)

    # If there are multiple of the same keys, remove all but the last.
    if results.length > 1
        results.each_with_index do |r, i|
            text.sub!(r.join(""), "") if i < results.length - 1
        end
    end

    text.sub!(results.last.join(""), "#{key}=#{value}\n")
end

def generate_slurm_header(job_name:, machine:, partition:, mins:, nodes:, ntasks_per_node: 1,
                          output: nil, dependencies:)
    # N.B. The "#SBATCH --export=" line means that only a few env. variables are preserved in
    # the job environment.
    stdout = output || "#{job_name}-%A.out"
    header = "#!/bin/bash -l
#SBATCH --job-name=#{job_name}
#SBATCH --output=#{stdout}
#SBATCH --nodes=#{nodes}
#SBATCH --ntasks-per-node=#{ntasks_per_node}
#SBATCH --time=#{mins2hms(mins)}
#SBATCH --clusters=#{machine}
#SBATCH --partition=#{partition}
#SBATCH --account=mwaops
#SBATCH --export=MWA_DIR,MWA_ASVO_API_KEY
"

    header << "#SBATCH --gres=gpu:1" << "\n" if partition == "gpuq"
    header << "
module use /group/mwa/software/modulefiles
#{dependencies.map { |d| "module load " << d }.join("\n")}

set -eux" << "\n"
end

def rts_version(path_to_git_commit_text_file)
    if File.exist?(path_to_git_commit_text_file)
        git_commit = File.readlines(path_to_git_commit_text_file).join
        "#{path_to_git_commit_text_file}\n\n#{git_commit}"
    else
        path
    end
end

def flag_tiles
    # A cheap, horrible hack until I can be bothered doing something more appropriate.
    # For now, just look at the bandpass calibration solutions.
    bp_output = `/group/mwaeor/cjordan/software/plot_BPcal_128T.py`
    bp_output.scan(/\(flag\s+(\d+)\?\)/).flatten.uniq.join("\n")
end

def check_download_status(path: ".", stdout_log: nil)
    stdout_log ||= Dir.glob("#{path}/ASVO_dl*.out").max_by { |l| File.mtime(l) }
    if stdout_log and not File.read(stdout_log).match?(/Download complete/)
        return ["failed", "stdout did not contain 'Download complete'"]
    end

    # Check the numbering of the gpubox files. There should be equal numbers
    # of *00.fits, *01.fits etc.
    gpubox_files = Dir.glob("*gpubox*.fits")
    num_coarse_bands = gpubox_files.map { |f| f.match(/gpubox(\d{2})/)[1] }
                                   .uniq.length
    n = 0
    loop do
        nn = format("%02d", n)
        num_of_n_gpubox_files = gpubox_files.select { |f| f.match(/gpubox\d{2}_#{nn}/) }.length
        break if num_of_n_gpubox_files.zero?
        unless gpubox_files.select { |f| f.match(/gpubox\d{2}_#{nn}/) }.length \
               == num_coarse_bands
            return ["failed", "Unequal number of gpubox files; are some missing?"]
        end

        n += 1
    end

    ["downloaded", "Download complete"]
end

def check_rts_status(path: ".", stdout_log: nil)
    # This function tries to determine the status of the RTS run based on a set
    # of heuristics. Note that if this function can't find any files for
    # `stdout_log`, it will assume the RTS run failed. If the RTS job is queued
    # or running, it would be wise to check if the log file(s) exist before
    # calling this function.
    stdout_log ||= Dir.glob("#{path}/RTS*.out").max_by { |l| File.mtime(l) }

    # If there's no log, then maybe the job didn't run.
    if not stdout_log
        status = "failed"
        final = "*** no stdout logs"
    # If it's too big, then it failed.
    elsif File.stat(stdout_log).size.to_f > 1_000_000
        status = "failed"
        final = "*** huge stdout - tiles probably need to be flagged."
    else
        # Inspect the contents of the STDOUT log for common errors.
        contents = File.read(stdout_log, :encoding => "UTF-8")
        begin
            [
                "CalibratorMeasurementLoop: There are no valid tiles",
                "DBC ERROR.*Could not open array file for reading",
                "FillMatrices: invalid tile pointing",
                "Cannot find metafits file",
                "TRACE: |- *, error 104",
                "Error: Unable to set weighted average frequency. No unflagged channels",
                "Fatal error in MPI_Init*",
            ].each do |error|
                next unless contents.match? error
                status = "failed"
                final = error
                return [status, final]
            end
        rescue ArgumentError
            return ["failed", "stdout file was not UTF-8 compliant."]
        end

        # Check for an "srun: error: " message. We can't do this above, because
        # this "srun: error: " message might arise from something fragile
        # (e.g. a python script), and we'd rather just check if the RTS itself
        # failed.
        contents.split("+ srun").each do |b|
            return ["failed", "RTS srun failed"] if \
                b.match?(/srun: error: /) and b.split("\n").first.match?(/ rts_... /)
        end

        # Read the latest node???.log file.
        node_log = Dir.glob("#{path}/*node???.log").max_by { |l| File.mtime(l) }
        if not node_log
            status = "failed"
            final = "*** no node logs"
        else
            # Read the last line of the log.
            final = File.readlines(node_log).last.strip
            status = if final.match?(/LogDone. Closing file/)
                         "succeeded"
                     else
                         "failed"
                     end
        end
    end
    [status, final]
end

# Convert two ionospheric statistics to a single number.
# See Jordan et al. 2017 for more information.
def iono_metric(mag:, pca:)
    m = mag.to_f
    eig = pca.to_f

    metric = 25 * m
    metric += 64 * eig * (eig - 0.6) if eig > 0.6
    return metric.to_s
end

def available_coarse_band_channels(path:)
    # Reliably determine which coarse-band channels are available for an
    # observation.
    files = Dir.glob("#{path}/*gpubox*.fits").sort
    files.map { |f| f.match(/gpubox(\d{2})(_\d{2})?.fits/)[1].to_i }.sort.uniq
end

class Obsid
    attr_reader :obsid,
                :type,
                :path,
                :download_jobid,
                :setup_jobid,
                :patch_jobid,
                :peel_jobid,
                :high_setup_jobid,
                :high_patch_jobid,
                :high_peel_jobid,
                :low_setup_jobid,
                :low_patch_jobid,
                :low_peel_jobid,
                :metafits,
                :timestamp_dir,
                :status,
                :final,
                :stdout_log,
                :node001_log,
                :subband_ids,
                :low_priority

    def initialize(obsid,
                   path: nil,
                   timestamp: true,
                   low_priority: false,
                   force_ra: nil,
                   force_dec: nil)
        # Sanity checks.
        # If force_ra is specified but not force_dec, or vice versa, then panic.
        if force_ra != force_dec
            abort("Only one of force_ra and force_dec was set! Set both.")
        end

        # "obsid" is (probably) a 10 digit string, representing the GPS timestamp
        # of an observation collected with the MWA.
        @obsid = obsid
        if path
            # If the path is manually specified, then it must already contain
            # the timestamp directory.
            a = path.split("/")
            abort "Manually specified path (#{path}) doesn't have the correct " \
                  "format in Obsid.initialize" \
                unless a.last.match?(/\d{4}-\d{2}-\d{2}_\d{4}/)
            @path = a[0..-2].join("/")
            @timestamp_dir = a.last
        else
            @path = "#{ENV["MWA_DIR"].chomp("/")}/data/#{obsid}"
            if timestamp
                date, time = Time.now.to_s.split[0..1]
                @timestamp_dir = [date, time.split(":")[0..1].join].join("_")
            else
                @timestamp_dir = ""
            end
        end

        @metafits = Dir.glob("#{@path}/*metafits*").max_by { |f| File.size(f) }
        @low_priority = low_priority
        @force_ra = if force_ra == -9999
                        nil
                    else
                        force_ra
                    end
        @force_dec = if force_dec == -9999
                         nil
                     else
                         force_dec
                     end
    end

    def obs_type
        # Use the RAPHASE header tag wherever available.
        # Handle the RAPHASE key not always being available; use RA instead.
        ra = read_fits_key(fits: @metafits, key: "RAPHASE")
        ra ||= read_fits_key(fits: @metafits, key: "RA")
        ra = ra.to_f
        filename = read_fits_key(fits: @metafits, key: "FILENAME")

        # "LymanA" refers to special split-band data. This could be handled more
        # generally.
        @type = if filename.include? "LymanA"
                    "LymanA"
                else
                    "RA=#{ra}"
                end
    end

    def download(asvo_job:, mins: 60, dl_path: @path, dependencies: nil)
        dependencies ||= %w[giant-squid]
        contents = generate_slurm_header(job_name: "dl_#{@obsid}",
                                         machine: "zeus",
                                         partition: "copyq",
                                         mins: mins,
                                         nodes: 1,
                                         output: "ASVO_dl-#{@obsid}-%A.out",
                                         dependencies: dependencies)
        contents << "
cd #{dl_path}
time giant-squid download -v #{asvo_job}
# Place an empty file to say that gator has been here.
touch ./.gator_download_complete
"

        FileUtils.mkdir_p @path unless Dir.exist? @path
        Dir.chdir @path unless Dir.pwd == @path
        write(file: "#{@obsid}.sh", contents: contents)
        @download_jobid = sbatch("#{@obsid}.sh", low_priority: @low_priority)
                          .match(/Submitted batch job (\d+)/)[1].to_i
    end

    def rts(setup_mins: 10,
            cal_mins: 0,
            patch: true,
            peel: true,
            patch_number: 1000,
            peel_number: 1000,
            rts_module_version: "master",
            srclists_module_version: "master",
            srclist: "srclist_pumav3_EoR0aegean_EoR1pietro+ForA.txt",
            download_metafits: true,
            coarse_bands: "all",
            use_existing_flags: true,
            flag_tiles: nil,
            fhd_flag_antennas: nil)
        # Sanity checking.
        abort "Cannot peel if we are not patching; exiting." if peel and not patch

        # Fail if we don't find any gpubox files *and* we're patching and/or peeling.
        if Dir.glob("#{@path}/*gpubox*").empty? and patch
            @status = "No gpubox files"
            return
        end

        @patch = patch
        @peel = peel
        @status = "OK"
        @setup_mins = setup_mins
        @cal_mins = cal_mins
        @patch_number = patch_number
        @peel_number = peel_number
        @rts_module_version = rts_module_version
        @srclists_module_version = srclists_module_version

        if download_metafits
            wget_output = "ERROR"
            error_message_shown = false
            while wget_output.match?("ERROR")
                wget_output = `wget "http://ws.mwatelescope.org/metadata/fits/?obs_id=#{@obsid}" -O #{@path}/#{@obsid}.metafits 2>&1`
                if wget_output.match?("ERROR")
                    unless error_message_shown
                        puts "Couldn't reach mwa-metadata server to download metafits! Will keep retrying..."
                        error_message_shown = true
                    end
                    sleep 60
                else
                    break
                end
            end
        end

        @metafits = Dir.glob("#{@path}/*metafits*").max_by { |f| File.mtime(f) }
        abort("#{@obsid}: metafits file not found!") unless @metafits

        # If the flags zip doesn't exist, then we'll need to generate our own
        # with cotter.
        if Dir.glob("#{@path}/#{@obsid}_flags.zip").empty?
            @cotter = true
            @setup_mins += 120
        else
            @cotter = false
        end

        # If the number of job minutes for calibration was not specified, then
        # adjust based on patching and peeling.
        if cal_mins.zero?
            @cal_mins += 15 if patch
            @cal_mins += 25 if peel
        end

        @int_time = read_fits_key(fits: @metafits, key: "INTTIME")
        if @int_time == "0.5"
            @corr_dumps_per_cadence_cal = 128
            @number_of_integration_bins_cal = 7

            @corr_dumps_per_cadence_peel = 16
            @number_of_integration_bins_peel = 5
        elsif @int_time == "1.0"
            @corr_dumps_per_cadence_cal = 64
            @number_of_integration_bins_cal = 7

            @corr_dumps_per_cadence_peel = 8
            @number_of_integration_bins_peel = 3
        elsif @int_time == "2.0"
            @corr_dumps_per_cadence_cal = 32
            @number_of_integration_bins_cal = 6

            @corr_dumps_per_cadence_peel = 4
            @number_of_integration_bins_peel = 3
        else
            abort("Unknown integration time! ('#{@int_time}' for #{@obsid})")
        end
        @number_of_iterations_cal = 1
        @number_of_iterations_peel = 14

        @channel_bandwidth = read_fits_key(fits: @metafits, key: "FINECHAN")
        unless @channel_bandwidth
            abort("FINECHAN key not in the metafits file (#{@metafits}) -- \n" \
                  "cannot determine channel bandwidth (aka fine channel frequency resolution)!")
        end
        @channel_bandwidth_khz = @channel_bandwidth.to_i
        @channel_bandwidth_mhz = @channel_bandwidth.to_f / 1000
        @flagged_channels_file = "#{File.dirname(__FILE__)}/templates/flagged_channels_" +
                                 case @channel_bandwidth_khz
                                 when 10
                                     "10khz.txt"
                                 when 20
                                     "20khz.txt"
                                 when 40
                                     "40khz.txt"
                                 else
                                     abort("No template flagged channels file for channel " \
                                           "bandwidth #{@channel_bandwidth_khz} kHz! " \
                                           "(obsid: #{@obsid})")
                                 end

        @flagged_tiles = Set[]
        # Check if a flagged_tiles.txt exists in the parent obsid directory. If
        # so, then use it; someone has put it there as the agreed-upon flagging.
        if use_existing_flags and File.exist?("#{@path}/flagged_tiles.txt")
            File.readlines("#{@path}/flagged_tiles.txt").each do |line|
                # There should be some checking here if a single int number is
                # on every line!
                @flagged_tiles.add(line.chomp.to_i)
            end
        end

        if flag_tiles
            flag_tiles.each { |f| @flagged_tiles.add(f) }
        end

        # Convert any FHD antenna flags to RTS tile flags with a little python script.
        if fhd_flag_antennas
            `fhd2rts_tile_flags.py -m #{@metafits} #{fhd_flag_antennas.join(" ")}`
                .split.map(&:to_i).each { |f| @flagged_tiles.add(f) }
        end

        # Determine the path to our source list based on the srclists module version.
        @source_list = srclist
        source_list_prefix = @source_list.split("/").last.split(".txt").first
        @patch_source_catalogue_file = "#{@path}/#{timestamp_dir}/#{source_list_prefix}_#{@obsid}_patch#{@patch_number}.txt"
        @peel_source_catalogue_file = "#{@path}/#{timestamp_dir}/#{source_list_prefix}_#{@obsid}_peel#{@peel_number+1000}.txt"

        # The RA needs to be in an hour angle format.
        # Handle the RAPHASE key not always being available; use RA instead.
        if @force_ra
            @obs_image_centre_ra = @force_ra
            @obs_image_centre_dec = @force_dec
        else
            ra = read_fits_key(fits: @metafits, key: "RAPHASE")
            ra ||= read_fits_key(fits: @metafits, key: "RA")
            @obs_image_centre_ra = (ra.to_f / 15.0).to_s
            @obs_image_centre_dec = read_fits_key(fits: @metafits, key: "DECPHASE")
            @obs_image_centre_dec ||= read_fits_key(fits: @metafits, key: "DEC")
        end

        # Run the "obs_type" function if the "type" attribute doesn't exist.
        obs_type unless @type
        # Here, we handle special observations. If the current "type" isn't listed here, it gets processed generically.
        if @type == "LymanA"
            # High band
            # Frequency of channel 24, if all bands were contiguous (identical here, i.e. channel 142)
            # This frequency is actually about half a channel lower in frequency, i.e. 141.49609*1.28MHz
            @obs_freq_base = 181.135
            @subband_ids = available_coarse_band_channels(path: @path).join(",")
            @timestamp_dir << "_high"
            @patch_source_catalogue_file = "#{@path}/#{timestamp_dir}/#{source_list_prefix}_#{@obsid}_patch#{@patch_number}.txt"
            @peel_source_catalogue_file = "#{@path}/#{timestamp_dir}/#{source_list_prefix}_#{@obsid}_peel#{@peel_number+1000}.txt"
            rts_setup(mins: @setup_mins)
            rts_patch(mins: cal_mins, peel: @peel, submit_job: @patch)
            @high_setup_jobid = @setup_jobid
            @high_patch_jobid = @patch_jobid

            # Low band
            # Frequency of channel 24, if all bands were contiguous (channel 150 here, even though our lowest is 158)
            # This frequency is actually about half a channel lower in frequency, i.e. 149.49609*1.28MHz
            @obs_freq_base = 191.355
            @subband_ids = available_coarse_band_channels(path: @path).join(",")
            @timestamp_dir = @timestamp_dir.split("_").select { |e| e =~ /\d/ }.join("_") << "_low"
            @patch_source_catalogue_file = "#{@path}/#{timestamp_dir}/#{source_list_prefix}_#{@obsid}_patch#{@patch_number}.txt"
            @peel_source_catalogue_file = "#{@path}/#{timestamp_dir}/#{source_list_prefix}_#{@obsid}_peel#{@peel_number+1000}.txt"
            rts_setup(mins: @setup_mins)
            rts_patch(mins: cal_mins, peel: @peel, submit_job: @patch)
            @low_setup_jobid = @setup_jobid
            @low_patch_jobid = @patch_jobid
        else
            # This is for all other "non-special" fields, including EoR fields.
            if coarse_bands == "all"
                @subband_ids = available_coarse_band_channels(path: @path).join(",")
            else
                @subband_ids = coarse_bands.join(",")
            end
            rts_setup(mins: @setup_mins, cotter: @cotter)
            rts_patch(mins: cal_mins, peel: @peel, submit_job: @patch)
        end
    end

    def rts_setup(mins: 10, cotter: false, dependencies: nil)
        dependencies ||= ["RTS/#{@rts_module_version}",
                          "srclists/#{@srclists_module_version}",
                          "cotter",
                          "mwapy",
                          "pyfits",
                          "pytz",
                          "scipy",
                          "astropy",
                          ]
        filename = "rts_setup.sh"
        contents = generate_slurm_header(job_name: "se_#{@obsid}",
                                         machine: "galaxy",
                                         partition: "gpuq",
                                         mins: mins,
                                         nodes: 1,
                                         output: "RTS-setup-#{@obsid}-%A.out",
                                         dependencies: dependencies)
        if cotter
            contents << "
cd ..
time cotter -o #{@obsid}_%%.mwaf -m #{@metafits} *gpubox*.fits -allowmissing -absmem 26
time zip -9 #{@obsid}_flags.zip #{@obsid}_??.mwaf
cd #{@timestamp_dir}
"
        end

        dummy_template_file = "<(echo \"#{File.dirname(__FILE__)}/templates/patch.in
#{File.dirname(__FILE__)}/templates/peel.in\")"

        contents << "
list_gpubox_files.py obsid.dat
ln -sf ../gpufiles_list.dat .

#####
# new way / getting rid off the wrapper generate_dynamic_RTS_sourcelists.py
#
# two commands feeding directly srclist_by_beam.py to pick randomly sources and attenuate them
# by the beam. one to feed the patch (calibrate data) and one to feed in peel (peel sources from data)
#####
srclist_by_beam.py -n #{@patch_number} \\
                   --srclist=#{@source_list} \\
                   --metafits=#{@metafits}
"
        if @peel
            contents << "
srclist_by_beam.py -n #{@peel_number+1000} \\
                   --srclist=#{@source_list} \\
                   --metafits=#{@metafits} \\
                   --no_patch \\
                   --cutoff=30
"
        end
        contents << "
#####

#####
# generate the rts_patch.sh file - to many options and stuff. should be more like rts_setup!
generate_mwac_qRTS_auto.py #{@path}/#{@timestamp_dir}/obsid.dat \\
                           #{ENV["USER"]} 24 \\
                           #{dummy_template_file} \\
                           --auto \\
                           --chunk_number=0 \\
                           --dynamic_sourcelist=1000 \\
                           --sourcelist=#{@source_list}
#####

reflag_mwaf_files.py #{@path}/#{@timestamp_dir}/obsid.dat

#####
# the following should be replaced by a template generation function from metafits header.
generate_RTS_in_mwac.py #{@path} \\
                        #{ENV["USER"]} 24 128T \\
                        --templates=#{dummy_template_file} \\
                        --header=#{@metafits} \\
                        --channel_flags=#{@flagged_channels_file}

mv #{ENV["USER"]}_rts_0.in #{ENV["USER"]}_rts_patch.in
mv #{ENV["USER"]}_rts_1.in #{ENV["USER"]}_rts_peel.in

sed -i \"s|\\(CorrDumpsPerCadence=\\).*|\\1#{@corr_dumps_per_cadence_cal}|\" #{ENV["USER"]}_rts_patch.in
sed -i \"s|\\(NumberOfIntegrationBins=\\).*|\\1#{@number_of_integration_bins_cal}|\" #{ENV["USER"]}_rts_patch.in
sed -i \"s|\\(NumberOfIterations=\\).*|\\1#{@number_of_iterations_cal}|\" #{ENV["USER"]}_rts_patch.in
sed -i \"s|//SourceCatalogueFile.*||; s|\\(SourceCatalogueFile=\\).*|\\1#{@patch_source_catalogue_file}|\" #{ENV["USER"]}_rts_patch.in
sed -i \"s|\\(doRFIflagging=\\).*|\\10|\" #{ENV["USER"]}_rts_patch.in
sed -i \"s|\\(ObservationImageCentreRA=\\).*|\\1#{@obs_image_centre_ra}|\" #{ENV["USER"]}_rts_patch.in
sed -i \"s|\\(ObservationImageCentreDec=\\).*|\\1#{@obs_image_centre_dec}|\" #{ENV["USER"]}_rts_patch.in
sed -i \"s|\\(SubBandIDs=\\).*|\\1#{@subband_ids}|\" #{ENV["USER"]}_rts_patch.in
sed -i \"s|\\(ChannelBandwidth=\\).*|\\1#{@channel_bandwidth_mhz}|\" #{ENV["USER"]}_rts_patch.in

sed -i \"s|\\(CorrDumpsPerCadence=\\).*|\\1#{@corr_dumps_per_cadence_peel}|\" #{ENV["USER"]}_rts_peel.in
sed -i \"s|\\(NumberOfIntegrationBins=\\).*|\\1#{@number_of_integration_bins_peel}|\" #{ENV["USER"]}_rts_peel.in
sed -i \"s|\\(NumberOfIterations=\\).*|\\1#{@number_of_iterations_peel}|\" #{ENV["USER"]}_rts_peel.in
sed -i \"s|//SourceCatalogueFile.*||; s|\\(SourceCatalogueFile=\\).*|\\1#{@peel_source_catalogue_file}|\" #{ENV["USER"]}_rts_peel.in
sed -i \"s|\\(doRFIflagging=\\).*|\\10|\" #{ENV["USER"]}_rts_peel.in
sed -i \"s|\\(ObservationImageCentreRA=\\).*|\\1#{@obs_image_centre_ra}|\" #{ENV["USER"]}_rts_peel.in
sed -i \"s|\\(ObservationImageCentreDec=\\).*|\\1#{@obs_image_centre_dec}|\" #{ENV["USER"]}_rts_peel.in
sed -i \"s|\\(SubBandIDs=\\).*|\\1#{@subband_ids}|\" #{ENV["USER"]}_rts_peel.in
sed -i \"s|\\(NumberOfSourcesToPeel=\\).*|\\1#{@peel_number}|\" #{ENV["USER"]}_rts_peel.in
sed -i \"s|\\(NumberOfIonoCalibrators=\\).*|\\1#{@peel_number}|\" #{ENV["USER"]}_rts_peel.in
sed -i \"s|\\(ChannelBandwidth=\\).*|\\1#{@channel_bandwidth_mhz}|\" #{ENV["USER"]}_rts_peel.in
"
        if @obs_freq_base
            contents << "
sed -i \"s|\\(ObservationFrequencyBase=\\).*|\\1#{@obs_freq_base}|\" #{ENV["USER"]}_rts_patch.in
sed -i \"s|\\(ObservationFrequencyBase=\\).*|\\1#{@obs_freq_base}|\" #{ENV["USER"]}_rts_peel.in
"
        end
        contents << "
echo gator rts_setup.sh finished successfully.
"

        FileUtils.mkdir_p "#{@path}/#{@timestamp_dir}"
        Dir.chdir "#{@path}/#{@timestamp_dir}"
        File.symlink(@metafits, File.basename(@metafits)) unless File.exist?(File.basename(@metafits))
        unless @flagged_tiles.empty?
            write(file: "#{@path}/#{@timestamp_dir}/flagged_tiles.txt",
                  contents: @flagged_tiles.to_a.join("\n"))
        end
        write(file: "obsid.dat", contents: @obsid)
        write(file: filename, contents: contents)
        @setup_jobid = sbatch(filename, low_priority: @low_priority)
                       .match(/Submitted batch job (\d+)/)[1].to_i
    end

    def rts_patch(mins: 15, peel: false, dependencies: nil, submit_job: true)
        # Peel is allowed to be redefined here for more flexibility.
        dependencies ||= ["RTS/#{@rts_module_version}"]

        num_nodes = @subband_ids.split(",").length + 1

        filename = "rts_patch.sh"
        contents = generate_slurm_header(job_name: "pa_#{@obsid}",
                                         machine: "galaxy",
                                         partition: "gpuq",
                                         mins: mins,
                                         nodes: num_nodes,
                                         output: "RTS-patch-#{@obsid}-%A.out",
                                         dependencies: dependencies)
        contents << "
command -v rts_gpu

date
srun -n #{num_nodes} --export=ALL rts_gpu #{ENV["USER"]}_rts_patch.in
date
srun -n 1 -N 1 --export=ALL plot_BPcal_128T.py --both --outname BPcal.png
set +e
srun -n 1 -N 1 --export=ALL plot_CalSols.py --base_dir=`pwd` -n #{@obsid}
set -e
date
"

        contents << "srun -n #{num_nodes} --export=ALL rts_gpu #{ENV["USER"]}_rts_peel.in
date
" if peel

        Dir.chdir "#{@path}/#{@timestamp_dir}" unless Dir.pwd == "#{@path}/#{@timestamp_dir}"
        write(file: filename, contents: contents)
        write(file: "rts_version_used.txt",
              contents: rts_version("/group/mwa/software/RTS/#{@rts_module_version}/galaxy/bin/git_commit.txt"))
        if submit_job
            return @patch_jobid = sbatch("--dependency=afterok:#{@setup_jobid} #{filename}",
                                         low_priority: @low_priority)
                                      .match(/Submitted batch job (\d+)/)[1].to_i
        end
        @patch_jobid = 0
    end

    def rts_peel(mins: 30, submit_job: true)
        num_nodes = @subband_ids.split(",").length + 1

        filename = "rts_peel.sh"
        contents = generate_slurm_header(job_name: "pe_#{@obsid}",
                                         machine: "galaxy",
                                         partition: "gpuq",
                                         mins: mins,
                                         nodes: num_nodes,
                                         output: "RTS-peel-#{@obsid}-%A.out")
        contents << "
date
srun -n #{num_nodes} --export=ALL rts_gpu #{ENV["USER"]}_rts_peel.in
"

        Dir.chdir "#{@path}/#{@timestamp_dir}" unless Dir.pwd == "#{@path}/#{@timestamp_dir}"
        write(file: filename, contents: contents)
        if submit_job
            return @peel_jobid = sbatch("--dependency=afterok:#{@patch_jobid} #{filename}",
                                        low_priority: @low_priority)
                                     .match(/Submitted batch job (\d+)/)[1].to_i
        end
        @peel_jobid = 0
    end

    def rts_status
        @status, @final_message = check_rts_status(path: "#{@path}/#{@timestamp_dir}")
    end
end
