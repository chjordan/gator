# This library file exists to encapsulate tedious, verbose database operations.

require "time"
require "sqlite3"

def update_db(db:,
              table_name:,
              obsid:,
              last_checked: Time.now,
              path: nil,
              job_id: nil,
              setup_job_id: nil,
              rts_job_id: nil,
              status: nil,
              stdout: nil,
              job_time: nil,
              new_path: nil,
              delete_after_cal: nil)
    # This function updates a database with whatever optional arguments are
    # supplied. The non-optional arguements are required to identify the correct
    # row(s) to be updated.

    # If some of the default-nil values are not nil, then throw them into the
    # SET area of the SQL update. Always update "LastChecked".
    settings = ["SET LastChecked = '#{last_checked}'"]
    settings << "    JobID = #{job_id}" unless job_id.nil?
    settings << "    SetupJobID = #{setup_job_id}" unless setup_job_id.nil?
    settings << "    RTSJobID = #{rts_job_id}" unless rts_job_id.nil?
    settings << "    Status = '#{status}'" unless status.nil?
    settings << "    Stdout = '#{stdout}'" unless stdout.nil?
    settings << "    Path = '#{new_path}'" unless new_path.nil?
    settings << "    JobTime = '#{job_time}'" unless job_time.nil?
    settings << "    DeleteAfterCal = #{delete_after_cal}" unless delete_after_cal.nil?

    # gator RTS calibration requires the `path` argument to be used, but not for
    # downloading. Tailor the WHERE part of the SQL update.
    where_condition = if path.nil?
                          "WHERE Obsid = #{obsid}"
                      else
                          "WHERE Obsid = #{obsid} AND Path = '#{path}'"
                      end

    op = ["UPDATE #{table_name}",
          settings.join(",\n"),
          where_condition].join("\n")
    db.execute(op)
end

def tables_in_db(db:)
    db.results_as_hash = true
    db.execute("SELECT name FROM sqlite_master WHERE type='table'")
      .map { |table| table.values.first }
end
