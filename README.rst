=======
 gator
=======
-------------------------------
GAlaxy Tools for Obsids and RTS
-------------------------------

Motivation
==========
Downloading obsids for the MWA can be a tedious, time-consuming task. The root of this problem lies with having the majority of data stored on tapes for long-term storage. `gator` aims to lessen the cognitive load required to download and process large amounts of data.

These scripts probably mean little to anyone not using Pawsey supercomputers, but they could be easily modified to work on other systems. I am, however, more interested in keeping things simple, and thus only supporting Pawsey systems for the moment.

Requirements
============
Environment variables
---------------------
- `MWA_DIR` - the directory in which data is downloaded. For EoR, this should be `/astro/mwaeor/MWA/`. Obsids are actually contained in `$MWA_DIR/data/`.
- ASVO details - your credentials created on `this website <https://asvo.mwatelescope.org/dashboard>`_, i.e. ::

    export ASVO_ASVO_API_KEY="********-****-****-****-************"

Software
--------
* Ensure that `module use /group/mwa/software/modulefiles` is in one of your login scripts (e.g. ~/.bashrc)
* `module load gator` loads all dependencies and places `gator` scripts into your `$PATH`.

Example profile scripts
-----------------------
~/.bashrc ::

  . ~/.profile

~/.profile ::

  module use /group/mwa/software/modulefiles
  module load gator
  module load tmux

  export MWA_DIR=/astro/mwaeor/MWA/
  export ASVO_USER="cjordan"
  export ASVO_PASS="******"

Why is your ~/.bashrc empty?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
You will understand when I convert you to `zsh <http://zsh.sourceforge.net/Intro/intro_1.html>`_.

Simple usage
============
`gator` is nominally used in two steps; these can be broadly thought of as "compile-time options" (job-specific instructions) and "run-time options" (queue-specific instructions). This will make more sense with the examples below.

Adding to database
------------------
`gator_add_to_database.rb` adds observations to a SQLite database. If you ask for help on its usage (i.e. `gator_add_to_database.rb -h`), you should get useful instructions: ::

  Usage: /group/mwa/software/gator/master/scripts/gator_add_to_database.rb [options] <--db /path/to/database/file> <file1/observation1> <file2/observation2> ...
  Add obsids to a database for downloading and/or RTS calibration.
      -h, --help                       Display this message.
          --db DATABASE                Specify the database to be used. No default.
      -d, --download-only              Do not queue specified observations for RTS calibration. Default: false
          --conversion-options STRING  Specify options for an ASVO cotter job. If unused, then gator will simply download the visibilities (default).
      -c, --calibrate-only             Do not queue specified observations for downloading. Default: false
          --[no-]patch                 Run the patch step. Default: true
          --[no-]peel                  Run the peel step. Default: true
          --[no-]delete                Delete gpubox files after RTS calibration (even if the RTS fails!). Default: false
          --[no-]timestamp             Isolate RTS calibration runs within timestamp directories. Default: true
          --patch-number NUM           Use this many sources in the patch step of the RTS. Default: 1000
          --peel-number NUM            Peel this many sources with the RTS. Default: 1000
          --dl-job-time NUM            Allow the download jobs to run for this many minutes. Default: 180
          --rts-job-time NUM           Allow the RTS jobs to run for this many minutes. Default: 60
          --rts-version STRING         Specify the module version to use for the RTS e.g. master Default: default
          --srclists-version STRING    Specify the module version to use for srclists e.g. master Default: default
      -s, --srclist STRING             Specify the source list to be used. Default: srclist_pumav3_EoR0aegean_EoR1pietro+ForA.txt
          --force-ra RA                Specify the RA to be used in calibration (decimal value of hour angle, e.g. 4.0). This ignores the RA value in metadata.
          --force-dec                  Specify the Dec to be used in calibration (decimal value, e.g. -30.0). This ignores the Dec value in metadata.

If you have a text file containing observations that you would like to download and calibrate, then the following is all you need: ::

  gator_add_to_database.rb --db=eor.sqlite obs.txt

It is assumed that the most used options are `-d`/`--download-only` and `-c`/`--calibrate-only`. So, if one wanted to download a single observation (e.g. 1065880128), the following command works: ::

  gator_add_to_database.rb -d --db=my_new_db.sqlite 1065880128

If data already exists at `$MWA_DIR` and you would like to calibrate it, then the following is suitable: ::

  gator_add_to_database.rb -c --db=my_second_new_db.sqlite 1065880128

Other options are available; see the help text. Also see `Advanced usage`_.

Daemon
------
`gator_daemon.rb` takes an SQLite database that you created earlier ("compiled" with your job options) and handles everything from there. If at any point you want to stop the daemon, just hit `Ctrl-C`. Note that this will *not* stop any jobs submitted to ASVO, zeus' copyq or galaxy's gpuq. As with `gator_add_to_database.rb`, the `-h` flag will list all possible options that `gator_daemon.rb` understands.

As `gator_daemon.rb` has to keep running to manage jobs in various states, it is a good idea to run it within something persistent like `tmux <https://github.com/tmux/tmux/wiki>`_ or `screen <https://www.gnu.org/software/screen/>`_. `tmux` is also available as a module.

Inspecting a database
---------------------
The contents of a database can be seen with `gator_read_table.rb`. To access the paths of successfully calibrated observations, the following command is useful: ::

  gator_read_table.rb --db 3c444.sqlite -t rts | grep succeeded | awk '{ print $NF }'

Advanced usage
==============
`gator_add_to_database.rb`
--------------------------
Multiple versions of the RTS as well as PUMA source lists exist. The default behaviour of `gator` is to use the latest versions of these software packages, but the versions can be specified at "compile time": ::

  gator_add_to_database.rb --rts-version 2018-05-28 --srclists-version 2018-05-18 --db secret_squirrels.sqlite obsids.txt

Module versions can be seen with `module avail`, i.e.::

  module avail RTS

RTS calibration is usually done in two steps: direction-independent ("patch") and direction-dependent ("peel"). It is possible to not do the expensive peel step: ::

  gator_add_to_database.rb --no-peel --db exploding_stars.sqlite obsids.txt

It is also possible to not run the RTS at all: ::

  gator_add_to_database.rb --no-patch --no-peel --db exploding_stars.sqlite obsids.txt

This is useful if you just want `gator` to provide SLURM scripts that it otherwise *would have* run.

`gator_daemon.rb`
-----------------
Things are pretty simple here; check out the help text to control the number of jobs submitted in different queues.

What does `gator` actually do?
==============================
"Compile-time options"
----------------------
`gator_add_to_database.rb` is a fairly basic script that just adds all specified observations into an SQLite database with the settings specified. Each row of the database contains the information needed to run that job; because it is possible to add to an already existing database, it is possible to have jobs with different settings in the same database. However, it is not possible to calibrate the same observation with different settings in the same table.

"Run-time options"
------------------
`gator_daemon.rb` -- the daemon script -- loops over every single observation in the download and RTS tables of the specified database. Concerning downloads, it submits jobs to ASVO for download, and waits for those to finish. Once ASVO has fetched an observation, `gator_daemon.rb` submits a job to zeus' copyq to actually download the observation. Once that job has finished, RTS calibration can begin. The number of jobs submitted to ASVO and zeus' copyq are controlled by command-line arguments to `gator_daemon.rb`.

RTS calibration begins only when your user does not have too many jobs in galaxy's qpuq (again, controlled by command-line arguments), and:

- a download is complete; or
- no downloads were requested, and therefore data is assumed to already be available.

For each observation, a "timestamp directory" is created in that observation's folder. This is to keep track of multiple calibrations of the same observation, and dis-entangle any RTS calibration products from the raw data. `gator` writes SLURM scripts into the timestamp directory, and launches jobs from these scripts.

Bugs? Inconsistencies?
======================
Probably! If you find something odd, let me know and I'll attempt to fix it ASAP. Pull requests are also welcome.

gator?
======
I'm bad at naming things. Sorry.
