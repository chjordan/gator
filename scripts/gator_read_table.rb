#!/usr/bin/env ruby

require "sqlite3"
require "optparse"
$LOAD_PATH.unshift "#{File.dirname(__FILE__)}/.."
require "gator"
require "db_operations"

options = {}
OptionParser.new do |opts|
    opts.banner = "Usage: #{__FILE__} [-d /path/to/database]\nDisplay the contents of a database."
    opts.on("-h", "--help", "Display this message.") do
        puts opts
        exit
    end

    opts.on("--db DATABASE",
            "Specify the database to be used. No default.") \
      { |o| options[:db] = o }

    options[:table] = "rts"
    # By default, issue a warning if a table hasn't been manually specified.
    options[:warning] = true
    opts.on("-t", "--table TABLE",
            "The table within the database to be printed. Default: #{options[:table]}") do |o|
        options[:table] = o
        options[:warning] = false
    end

    options[:list_tables?] = false
    opts.on("-l", "--[no-]list-tables",
            "Just list the tables in this database and exit.") \
      { |o| options[:list_tables?] = o }

    options[:rows] = Float::INFINITY
    opts.on("-n", "--number-of-rows NUMBER",
            "Specify the database rows to be printed. By default, all rows are printed.") \
      { |o| options[:rows] = o.to_i }

    options[:verbose] = false
    opts.on("-v", "--verbose",
            "List all columns of the database in the output.") \
      { options[:verbose] = true }

    options[:paths] = false
    opts.on("-p", "--paths",
            "List only the paths to successful RTS jobs.") \
      { options[:paths] = true }

    options[:failures] = false
    opts.on("-f", "--failures",
            "When only listing RTS job paths, also list the failures.") \
      { options[:failures] = true }
end.parse!

abort("No database specified! Use --db") unless options[:db]
abort("Database doesn't exist!") unless File.exist?(options[:db])

db = SQLite3::Database.open options[:db]
db.results_as_hash = true
tables = tables_in_db(db: db)

if options[:list_tables?]
    puts "Database #{options[:db]} has tables: #{tables.join(", ")}"
    exit
end

begin
    if options[:warning] and not tables.include? options[:table]
        table_to_use = tables.first
        STDERR.puts "*** Warning: Table \"#{options[:table]}\" not found in database! Using \"#{table_to_use}\" instead.".yellow
        options[:table] = table_to_use
    elsif options[:warning]
        STDERR.puts "*** Warning: Table not specified; using table \"#{options[:table]}\".".yellow
    end

    count = 0
    db.execute("SELECT * FROM #{options[:table]}") do |r|
        if options[:verbose]
            puts r
        elsif options[:paths] and not options[:failures]
            puts r["Path"] if r["Status"] == "succeeded"
        elsif options[:paths]
            puts r["Path"]
        else
            puts "#{r["Obsid"]} \t #{r["Status"]} \t #{r["LastChecked"]} \t #{r["Stdout"]} \t #{r["Path"]}"
        end
        count += 1
        break if count > options[:rows]
    end
rescue SQLite3::Exception => e
    puts "#{e.class}: #{e}"
ensure
    db&.close
end
