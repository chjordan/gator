#!/usr/bin/env ruby

# TODO: Rename this script and handle failed downloads too.

# TODO: Check if "timestamp" directories aren't being used. If they aren't, then
# this script might nuke the whole obsid directory!

# This script deletes all of the failed RTS runs from a table and re-labels the
# observations as "unqueued", such that running the daemon on the table again
# will allow you to have another go at calibration.

require "optparse"
require "fileutils"
require "sqlite3"

$LOAD_PATH.unshift "#{File.dirname(__FILE__)}/.."
require "gator"

options = {}
OptionParser.new do |opts|
    opts.banner = "Usage: #{__FILE__} [options] <obsids>" \
                  "\nGiven a list of obsids, report the latest timestamp " \
                  "directory containing a successful RTS calibration."
    opts.on("-h", "--help", "Display this message.") do
        puts opts
        exit
    end

    options[:just_latest?] = false
    opts.on("-l", "--just-latest", "Don't check timestamp directories for successful " \
                                   "RTS calibration; just report the latest one. "\
                                   "Default: #{options[:just_latest?]}") \
    { options[:just_latest?] = true }

    options[:ignore_failures?] = false
    opts.on("-i", "--ignore-failures", "Don't print a message for obsids without a " \
                                       "successful RTS calibration. " \
                                       "Default: #{options[:just_latest?]}") \
    { options[:ignore_failures?] = true }

    options[:debug?] = false
    opts.on("--debug", "Print debug messages. Default: #{options[:debug?]}") \
    { options[:debug?] = true }
end.parse!

# Sanity checks.
abort("$MWA_DIR not defined.") unless ENV["MWA_DIR"]
abort("$MWA_DIR doesn't have a trailing slash! (e.g. /astro/mwaeor/MWA/)") unless ENV["MWA_DIR"][-1] == "/"

# Put the paths into arrays, such that all successes can be printed before failures.
successes = []
failures = []
obtain_obsids(ARGV).each do |obsid|
    path = "#{ENV["MWA_DIR"].chomp("/")}/data/#{obsid}"
    timestamps = Dir.glob("#{path}/2???-??-??_????").sort

    latest_timestamp = nil
    status = nil
    if options[:just_latest?]
        latest_timestamp = timestamps.last
    else
        timestamps.reverse.each do |t|
            status, final = check_rts_status(path: t)
            if options[:debug?]
                puts "obsid: #{obsid} timestamp: #{t} \ngator RTS status: #{status} final line: #{final}"
            end
            if status
                latest_timestamp = t
                break
            end
        end
    end

    if status == "succeeded"
        successes.append(latest_timestamp)
    else
        failures.append(obsid)
    end
end

puts successes.join("\n")
if not failures.empty? and not options[:ignore_failures?]
    puts "\nThese obsids have no successful RTS calibrations:"
    puts failures.join("\n")
end
