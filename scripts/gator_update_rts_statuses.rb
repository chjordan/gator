#!/usr/bin/env ruby

require "sqlite3"
require "optparse"
$LOAD_PATH.unshift "#{File.dirname(__FILE__)}/.."
require "gator"
require "db_operations"

options = {}
OptionParser.new do |opts|
    opts.banner = "Usage: #{__FILE__} [options] <--db /path/to/database/file> " \
                  "\nRun the check_rts_status over all timestamp directories to ensure " \
                  "consistency. Useful when check_rts_status has been updated."
    opts.on("-h", "--help", "Display this message.") do
        puts opts
        exit
    end

    options[:db] = nil
    opts.on("--db DATABASE", "Specify the database to be used. No default.") \
      { |o| options[:db] = o }

    options[:verbose?] = false
    opts.on("-v", "--verbose",
            "Print the old and new statuses if a change is needed. " \
              "Default: #{options[:verbose?]}") \
      { |o| options[:verbose?] = o }

    options[:min] = 1000000000
    opts.on("--min OBSID", "Only check obsids with values bigger than this.") \
      { |o| options[:min] = o.to_i }

    options[:max] = 9999999999
    opts.on("--max OBSID", "Only check obsids with values smaller than this.") \
      { |o| options[:max] = o.to_i }
end.parse!

# Sanity checks.
abort("No database specified! Use --db") unless options[:db]
abort("Database (#{options[:db]}) doesn't exist!") unless File.exist?(options[:db])

db = SQLite3::Database.open options[:db]
db.results_as_hash = true

db.transaction
db.execute("SELECT * FROM rts") do |r|
    next if r["Status"] == "unqueued" or \
            r["Obsid"] < options[:min] or \
            r["Obsid"] > options[:max]

    status, final = check_rts_status(path: r["Path"])
    if options[:verbose?] and r["Stdout"] != final
        puts "#{r["Obsid"]}:\n\tOld: '#{r["Status"]}' '#{r["Stdout"]}'\n\tNew: '#{status}' '#{final}'"
    end
    update_db(db: db, table_name: "rts", obsid: r["Obsid"], status: status, stdout: final)
end
db.commit if db.transaction_active?
