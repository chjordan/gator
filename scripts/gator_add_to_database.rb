#!/usr/bin/env ruby

require "sqlite3"
require "optparse"
$LOAD_PATH.unshift "#{File.dirname(__FILE__)}/.."
require "gator"
require "common_cli_args"
require "db_operations"

options = parse_table_cli_args
# Sanity checks.
abort("No observations specified!") if ARGV.empty?
abort("No database specified! Use --db") unless options[:db]

puts "Settings:
  --db:\t\t\t #{options[:db]}"

if options[:download_only?]
    puts "  --download-only?\t #{options[:download_only?]}".red
else
    puts "  --download-only?\t #{options[:download_only?]}"
end
puts "  --conversion-options:\t #{options[:conversion_options] || "N/A"}"

if options[:calibrate_only?]
    puts "  --calibrate-only?\t #{options[:calibrate_only?]}".red
else
    puts "  --calibrate-only?\t #{options[:calibrate_only?]}"
end

if not options[:download_only?]
  puts "  --patch?\t\t #{options[:patch?]}
  --peel?\t\t #{options[:peel?]}
  --delete?\t\t #{options[:delete_after_calibration?]}
  --timestamp?\t\t #{options[:timestamp?]}
  --patch-number:\t #{options[:patch_number]}
  --peel-number:\t #{options[:peel_number]}
  --dl-job-time:\t #{options[:download_job_time]}
  --rts-job-time:\t #{options[:rts_job_time]}
  --rts-version:\t #{options[:rts_version]}
  --srclists-version:\t #{options[:srclists_version]}
  --srclist:\t\t #{options[:srclist]}
  --force-ra:\t\t #{options[:force_ra] || "N/A"}
  --force-dec:\t\t #{options[:force_dec] || "N/A"}"
end
puts ""

# If we are doing any downloading, the following code applies.
if not options[:calibrate_only?]
    # Old tables that gator used were always called "downloads". Now use "dl" for downloads.
    table_name = "dl"

    db = if File.exist?(options[:db])
             SQLite3::Database.open options[:db]
         else
             SQLite3::Database.new options[:db]
         end
    unless tables_in_db(db: db).include? table_name
        db.execute "CREATE TABLE #{table_name}(" \
                   "Obsid INTEGER PRIMARY KEY, " \
                   "Ready INTEGER, " \
                   "Status TEXT, " \
                   "LastChecked TEXT, " \
                   "JobID INTEGER, " \
                   "Stdout TEXT, " \
                   "ConversionOptions TEXT, " \
                   "JobTime INTEGER)"
    end
    db.results_as_hash = true

    begin
        # Add specified obsids.
        obsids_in_table = db.execute("SELECT * FROM #{table_name}").map { |r| r[0] }
        added = 0
        db.transaction
        obtain_obsids(ARGV).each do |o|
            if obsids_in_table.include? o
                puts "Already queued for downloading: #{o.to_s.yellow}"
                next
            end

            obsids_in_table.push o
            db.execute "INSERT INTO #{table_name} VALUES(" \
                       "#{o.to_i}, " \
                       "0, " \
                       "'unqueued', " \
                       "'#{Time.now}', " \
                       "0, " \
                       "' ', " \
                       "'#{options[:conversion_options] || ""}', " \
                       "#{options[:download_job_time]})"
            added += 1
        end
        db.commit if db.transaction_active?
        puts "#{added} observations added for downloading."
    rescue SQLite3::Exception => e
        puts "#{e.class}: #{e}"
    ensure
        db&.close
    end
end

# If we are doing any RTS calibration, the following code applies.
if not options[:download_only?]
    table_name = "rts"

    db = if File.exist?(options[:db])
             SQLite3::Database.open options[:db]
         else
             SQLite3::Database.new options[:db]
         end
    unless tables_in_db(db: db).include? table_name
        db.execute "CREATE TABLE #{table_name}(" \
                   "id INTEGER PRIMARY KEY, " \
                   "Obsid INTEGER, " \
                   "Path TEXT, " \
                   "Status TEXT, " \
                   "LastChecked TEXT, " \
                   "SetupJobID INTEGER, " \
                   "RTSJobID INTEGER, " \
                   "Stdout TEXT, " \
                   "Timestamp INTEGER, " \
                   "Patch INTEGER, " \
                   "Peel INTEGER, " \
                   "PatchNumber INTEGER, " \
                   "PeelNumber INTEGER, " \
                   "JobTime INTEGER, " \
                   "RTSVersion TEXT, " \
                   "SrclistsVersion TEXT, " \
                   "Srclist TEXT, " \
                   "DeleteAfterCal INTEGER, " \
                   "ForceRA REAL, " \
                   "ForceDec REAL)"
    end
    db.results_as_hash = true

    begin
        obsids_in_table = db.execute("SELECT * FROM #{table_name}").map { |r| r[0] }
        # Add specified obsids.
        added = 0
        db.transaction
        obtain_obsids(ARGV).each do |o|
            if obsids_in_table.include? o
                puts "Already queued for RTS calibration: #{o.to_s.yellow}"
                next
            end
            obsids_in_table.push o

            db.execute "INSERT INTO #{table_name} VALUES(" \
                       "null, " \
                       "#{o}, " \
                       "' ', " \
                       "'unqueued', " \
                       "'#{Time.now}', " \
                       "0, " \
                       "0, " \
                       "' ', " \
                       "#{options[:timestamp?] ? 1 : 0}, " \
                       "#{options[:patch?] ? 1 : 0}, " \
                       "#{options[:peel?] ? 1 : 0}, " \
                       "#{options[:patch_number]}, " \
                       "#{options[:peel_number]}, " \
                       "#{options[:rts_job_time]}, " \
                       "'#{options[:rts_version]}', " \
                       "'#{options[:srclists_version]}', " \
                       "'#{options[:srclist]}', " \
                       "#{options[:delete_after_calibration?] ? 1 : 0}, " \
                       "#{options[:force_ra] || -9999}, " \
                       "#{options[:force_dec] || -9999})"
            added += 1
        end
        db.commit if db.transaction_active?
        puts "#{added} observations added for RTS calibration."
    rescue SQLite3::Exception => e
        puts "#{e.class}: #{e}"
    ensure
        db&.close
    end
end
