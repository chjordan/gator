#!/usr/bin/env ruby

# This script is intended to run the RTS for every obsid specified.
# It assumes that the data for each obsid has already been downloaded.

$LOAD_PATH.unshift "#{File.dirname(__FILE__)}/.."
require "gator"
require "common_cli_args"

options = parse_table_cli_args

abort "No obsids supplied!" if ARGV.length == 0

obtain_obsids(ARGV).each do |obsid|
    o = Obsid.new(obsid)
    o.rts(setup_mins: 10,
          cal_mins: 60,
          patch: options[:patch?],
          peel: options[:peel?],
          patch_number: options[:patch_number],
          peel_number: options[:peel_number],
          rts_module_version: options[:rts_version],
          srclists_module_version: options[:srclists_version],
          srclist: options[:srclist],
          download_metafits: true,
          coarse_bands: options[:coarse_bands],
         )
    puts "#{obsid}: #{o.type}"
end
