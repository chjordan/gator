#!/usr/bin/env ruby

require "sqlite3"
require "optparse"
$LOAD_PATH.unshift "#{File.dirname(__FILE__)}/.."
require "gator"
require "db_operations"

options = {}
OptionParser.new do |opts|
    opts.banner = "Usage: #{__FILE__} [options] <--db /path/to/database/file> " \
      "\nDownload and/or calibrate all of the obsids in the specified database."
    opts.on("-h", "--help", "Display this message.") do
        puts opts
        exit
    end

    options[:db] = nil
    opts.on("--db DATABASE", "Specify the database to be used. No default.") \
      { |o| options[:db] = o }

    options[:verbose?] = false
    opts.on("-v", "--verbose",
            "Print status messages every time the daemon loops. " \
              "Default: #{options[:verbose?]}") \
      { |o| options[:verbose?] = o }

    options[:debugging?] = false
    opts.on("--debug",
            "Print debugging messages. Default: #{options[:debugging?]}") \
      { |o| options[:debugging?] = o }

    options[:buffer_size] = 20
    opts.on("-b", "--buffer-size NUM",
            "Maximum number of obsids allowed to be downloaded that are currently not being RTS calibrated. " \
              "Default: #{options[:buffer_size]}") \
      { |o| options[:buffer_size] = o.to_i }

    options[:sleep_time] = 60
    opts.on("-s", "--sleep-time TIME",
            "Number of seconds to wait before re-analysing queues and jobs. " \
              "Default: #{options[:sleep_time]}") \
      { |o| options[:sleep_time] = o.to_i }

    options[:max_dl_queue_length] = 20
    opts.on("--copyq NUM",
            "The maximum number of jobs queued for downloading. " \
              "Default: #{options[:max_dl_queue_length]}") \
      { |o| options[:max_dl_queue_length] = o.to_i }

    options[:max_rts_queue_length] = 12
    opts.on("--gpuq NUM",
            "The maximum number of jobs queued for RTS calibration. " \
              "Default: #{options[:max_rts_queue_length]}") \
      { |o| options[:max_rts_queue_length] = o.to_i }

    options[:low_job_priority?] = false
    opts.on("--low-priority",
            "Submit copyq and qpuq jobs with a priority of 1; useful if you only want jobs to " \
            "run when the queues are empty. Default: #{options[:low_job_priority?]}") \
      { options[:low_job_priority?] = true }

    options[:use_existing_flags] = true
    opts.on("--dont-use-existing-flags",
            "Don't use the flagged_tiles.txt in the parent obsid directory.") \
      { options[:use_existing_flags] = false }

    options[:fhd_antenna_flags] = nil
    opts.on("--fhd-antenna-flags FILE",
            "The path to a file containing FHD antenna flags. The first value must be the obsid.") \
      { |o| options[:fhd_antenna_flags] = o }
end.parse!

# Sanity checks.
abort("No database specified! Use --db") unless options[:db]
abort("Database (#{options[:db]}) doesn't exist!") unless File.exist?(options[:db])
abort("--buffer-size cannot be 0 or negative!") if options[:buffer_size] <= 0
abort("--copyq cannot be 0 or negative!") if options[:max_dl_queue_length] <= 0
abort("$MWA_DIR not defined.") unless ENV["MWA_DIR"]
abort("$MWA_DIR doesn't have a trailing slash! (e.g. /astro/mwaeor/MWA/)") unless ENV["MWA_DIR"][-1] == "/"
abort("$MWA_ASVO_API_KEY not defined.") unless ENV["MWA_ASVO_API_KEY"]
abort("$USER not defined.") unless ENV["USER"]

db = SQLite3::Database.open options[:db]
db.results_as_hash = true

if options[:fhd_antenna_flags]
    fhd = {}
    File.readlines(options[:fhd_antenna_flags]).each do |line|
        s = line.split
        # If we don't skip lines that only contain an obsid, they will be
        # associated with an empty array, which ruby does not treat as nil.
        next if s.length == 1
        fhd[s[0].to_i] = s[1..-1].map(&:to_i)
    end
end

puts "gator_daemon.rb running".green

# Loop forever, until all obsids have been downloaded and/or calibrated.
begin
    loop do
        tables = tables_in_db(db: db)
        # If `any_update` is true here, then status messages are printed every time we loop.
        # Otherwise, status messages can only be printed under certain conditions;
        # see blocks with `any_update = true`.
        any_update = options[:verbose?]

        if tables.include? "dl"
            num_not_downloaded = db.execute("SELECT COUNT(*) FROM dl " \
                                            "WHERE NOT Status = 'downloaded' " \
                                            "  AND NOT Status = 'failed'")[0][0]

            _, jobs_in_queue, len_zeus_queue = get_queue(machine: "zeus",
                                                         partition: "copyq",
                                                         user: ENV["USER"])

            asvo_dict = asvo_queue
            num_asvo_processing = asvo_dict.map do |_, d|
                d.select { |_, v| v[1] == "Processing" }.length
            end.sum
            num_asvo_queued = asvo_dict.map do |_, d|
                d.select { |_, v| v[1] == "Queued" }.length
            end.sum

            db.execute("SELECT * FROM dl WHERE Status NOT IN ('failed', 'downloaded')") do |r|
                obsid = r["Obsid"]
                status = r["Status"]
                asvo = if r["ConversionOptions"].empty?
                           asvo_dict[:download]
                       else
                           asvo_dict[:conversion]
                       end

                # This conditional handles the copyq downloading from ASVO.
                if status == "copyq downloading"
                    jobid = r["JobID"]
                    next if jobs_in_queue.include? jobid
                    path = "#{ENV["MWA_DIR"]}/data/#{obsid}"
                    new_status, final = check_download_status(path: path,
                                                              stdout_log: "#{path}/ASVO_dl-#{obsid}-#{jobid}.out")
                    any_update = true
                    if new_status == "downloaded"
                        puts "#{obsid.to_s.green}: #{final}"
                    else
                        puts "#{obsid.to_s.red}: #{final}"
                    end
                    update_db(db: db, table_name: "dl",
                              obsid: obsid, stdout: final, status: new_status)
                # This conditional handles ASVO download jobs that have potentially finished.
                elsif status == "asvo downloading"
                    # We can't do anything further if we're already at the maximum queue length, or
                    # we've already got lots of ASVO jobs queued.
                    if len_zeus_queue >= options[:max_dl_queue_length]
                        if options[:debugging?]
                            puts "Breaking in \"status == asvo downloading\", because:\n" \
                                 "\tlen_zeus_queue >= options[:max_dl_queue_length]"
                        end
                        break
                    end
                    # Break when the buffer size is exceeded.
                    num_rts_running = if tables.include? "rts"
                                          db.execute("SELECT COUNT(Obsid) FROM rts " \
                                                     "WHERE Status IN ('setting up', 'rts running')")[0][0]
                                      else
                                          0
                                      end
                    if num_rts_running + len_zeus_queue > options[:buffer_size]
                        if options[:debugging?]
                            puts "Breaking in \"elsif status == asvo downloading\", because:\n" \
                                 "\tnum_rts_running + len_zeus_queue > options[:buffer_size]"
                        end
                        break
                    end

                    # Check the ASVO download.
                    # Re-download observations that are not in the ASVO queue.
                    if not asvo.include? obsid.to_s
                        conversion_options = r["ConversionOptions"]
                        asvo_job = if conversion_options.empty?
                                       submit_asvo_download(obsid: obsid)
                                   else
                                       submit_asvo_conversion(obsid: obsid,
                                                              options_string: conversion_options)
                                   end
                        any_update = true
                        puts "Submitted #{obsid.to_s.blue} as ASVO job #{asvo_job.to_s.blue}"
                        update_db(db: db, table_name: "dl",
                                  obsid: obsid, status: "asvo downloading")
                    # If it's ready, then we can submit a copyq download job.
                    elsif asvo[obsid.to_s][1].match? "Ready"
                        obj = Obsid.new(obsid, low_priority: options[:low_job_priority?])
                        obj.download(asvo_job: asvo[obsid.to_s][0], mins: r["JobTime"])
                        jobid = obj.download_jobid
                        any_update = true
                        puts "Submitted #{obsid.to_s.blue} as copyq job #{jobid.to_s.blue}"
                        update_db(db: db, table_name: "dl",
                                  obsid: obsid, job_id: jobid, status: "copyq downloading")
                        len_zeus_queue += 1
                    # This conditional describes an issue with the archive that we have no control
                    # over. Instead of failing here, set a couple of variables so that we fail
                    # later in this loop.
                    elsif asvo[obsid.to_s][1].match? "Error: Error staging data. Max retries reached.;"
                        puts "#{obsid.to_s.red}: ASVO archive error"
                        update_db(db: db, table_name: "dl",
                                  obsid: obsid, stdout: "ASVO archive error", status: "failed")
                        options[:buffer_size] = 0
                        options[:max_dl_queue_length] = 0
                    elsif asvo[obsid.to_s][1].match? "Error"
                        any_update = true
                        puts "#{obsid.to_s.red}: ASVO error"
                        update_db(db: db, table_name: "dl",
                                  obsid: obsid, stdout: "ASVO error", status: "failed")
                    end

                # This conditional handles submitting a download job to ASVO.
                elsif status == "unqueued"
                    # Check if gator has already downloaded this observation.
                    files = Dir.glob("#{ENV["MWA_DIR"]}/data/#{obsid}/*", File::FNM_DOTMATCH)
                               .map { |f| File.basename(f) }
                    conversion_options = r["ConversionOptions"]

                    if conversion_options.empty? \
                      and files.include?(".gator_download_complete") \
                      and not files.select { |f| f.include? "gpubox" }.empty?
                        new_status, final = check_download_status(path: "#{ENV["MWA_DIR"]}/data/#{obsid}")
                        any_update = true
                        if new_status == "downloaded"
                            puts "#{obsid.to_s.green}: #{final}"
                        else
                            puts "#{obsid.to_s.red}: #{final}"
                        end
                        update_db(db: db, table_name: "dl",
                                  obsid: obsid, stdout: final, status: new_status)

                        # Don't delete these data, as they were *already* downloaded.
                        if tables.include? "rts"
                            update_db(db: db, table_name: "rts",
                                      obsid: obsid, delete_after_cal: 0)
                        end
                    elsif not conversion_options.empty? \
                      and files.include?(".gator_download_complete") \
                      and files.include?("#{obsid}.ms")
                        new_status, final = check_download_status(path: "#{ENV["MWA_DIR"]}/data/#{obsid}")
                        any_update = true
                        if new_status == "downloaded"
                            puts "#{obsid.to_s.green}: #{final}"
                        else
                            puts "#{obsid.to_s.red}: #{final}"
                        end
                        update_db(db: db, table_name: "dl",
                                  obsid: obsid, stdout: final, status: new_status)

                        # Don't delete these data, as they were *already* downloaded.
                        if tables.include? "rts"
                            update_db(db: db, table_name: "rts",
                                      obsid: obsid, delete_after_cal: 0)
                        end
                    # Check if the download job already exists in ASVO.
                    elsif asvo.include? obsid.to_s
                        # If the ASVO status is downloading, then we just need to update
                        # our database.
                        if %w[Queued Processing].include? asvo[obsid.to_s][1]
                            update_db(db: db, table_name: "dl",
                                      obsid: obsid, status: "asvo downloading")
                        elsif asvo[obsid.to_s][1] == "Ready"
                            # We can't do anything further if we're already at the maximum queue length.
                            if len_zeus_queue >= options[:max_dl_queue_length]
                                if options[:debugging?]
                                    puts "Breaking in \"asvo.include? obsid.to_s\", because:\n" \
                                         "\tlen_zeus_queue >= options[:max_dl_queue_length]"
                                end
                                break
                            end
                            # Break when the buffer size is exceeded.
                            if tables.include? "rts"
                                num_rts_unqueued = db.execute("SELECT COUNT(Obsid) FROM rts WHERE Status = 'unqueued'")[0][0]
                                unless num_rts_unqueued - num_not_downloaded < options[:buffer_size]
                                    if options[:debugging?]
                                        puts "Breaking in \"asvo.include? obsid.to_s\", because:\n" \
                                             "\tnum_rts_unqueued - num_not_downloaded < options[:buffer_size]"
                                    end
                                    break
                                end
                            else
                                unless num_not_downloaded < options[:buffer_size]
                                    if options[:debugging?]
                                        puts "Breaking in \"asvo.include? obsid.to_s\", because:\n" \
                                             "\tnum_not_downloaded < options[:buffer_size]"
                                    end
                                    break
                                end
                            end

                            obj = Obsid.new(obsid, low_priority: options[:low_job_priority?])
                            obj.download(asvo_job: asvo[obsid.to_s][0], mins: r["JobTime"])
                            jobid = obj.download_jobid
                            any_update = true
                            puts "Submitted #{obsid.to_s.blue} as copyq job #{jobid.to_s.blue}"
                            update_db(db: db, table_name: "dl",
                                      obsid: obsid, job_id: jobid, status: "copyq downloading")
                            len_zeus_queue += 1
                        end

                    else
                        # Submit more jobs to ASVO *only* when there are a certain number
                        # of RTS calibration jobs left.
                        num_dl_unqueued = db.execute("SELECT COUNT(Obsid) FROM dl " \
                                                     "WHERE NOT Status = 'downloaded' AND NOT Status = 'failed'")[0][0]
                        num_rts_unqueued = if tables.include? "rts"
                                              db.execute("SELECT COUNT(Obsid) FROM rts " \
                                                         "WHERE Status = 'unqueued'")[0][0]
                                          else
                                              0
                                          end
                        if num_rts_unqueued - num_dl_unqueued > options[:buffer_size]
                            if options[:debugging?]
                                puts "Breaking in \"else (line 284)\", because:\n" \
                                     "\tnum_rts_unqueued (#{num_rts_unqueued}) - num_dl_unqueued (#{num_dl_unqueued}) > options[:buffer_size] (#{options[:buffer_size]})"
                            end
                            break
                        end

                        num_downloading = db.execute("SELECT COUNT(Obsid) FROM dl " \
                                                     "WHERE Status IN ('asvo downloading', 'copyq downloading')")[0][0]
                        if num_downloading > options[:buffer_size]
                            if options[:debugging?]
                                puts "Breaking in \"else (line 294)\", because:\n" \
                                     "\tnum_downloading (#{num_downloading}) > options[:buffer_size] (#{options[:buffer_size]})"
                            end
                            break
                        end

                        conversion_options = r["ConversionOptions"]
                        asvo_job = if conversion_options.empty?
                                       submit_asvo_download(obsid: obsid)
                                   else
                                       submit_asvo_conversion(obsid: obsid,
                                                              options_string: conversion_options)
                                   end
                        any_update = true
                        puts "Submitted #{obsid.to_s.blue} as ASVO job #{asvo_job.to_s.blue}"
                        update_db(db: db, table_name: "dl",
                                  obsid: obsid, status: "asvo downloading")
                    end
                end
            end

            # Handling MWA archive issues by aborting here.
            abort("MWA archive issue detected; resume #{__FILE__} once the archive is healthy again.") \
              if options[:buffer_size].zero? and options[:max_dl_queue_length].zero?
        end

        if tables.include? "rts"
            _, jobs_in_queue, len_galaxy_queue = get_queue(machine: "galaxy",
                                                           partition: "gpuq",
                                                           user: ENV["USER"])
            if tables.include? "dl"
                download_statuses = db.execute("SELECT Obsid, Status FROM dl")
                                      .map { |h| [h["Obsid"], h["Status"]] }
                                      .to_h
            end

            db.execute("SELECT * FROM rts") do |r|
                obsid = r["Obsid"]
                path = r["Path"]
                status = r["Status"]
                setup_job_id = r["SetupJobID"]
                rts_job_id = r["RTSJobID"]
                new_status = nil

                # If this obsid succeeded or failed, advance.
                next if ["succeeded", "failed", "no data", "???", "No gpubox files"].include? status

                # Handle the various states that a job could be in.
                if status == "unqueued"
                    # Leave the proper handling of unqueued jobs below; we do this here just to skip
                    # ahead.
                elsif jobs_in_queue.include? setup_job_id
                    new_status = "setting up" if status != "setting up"
                elsif jobs_in_queue.include? rts_job_id
                    new_status = "rts running" if status != "rts running"
                # The conditions below are only checked if both the setup_ and rts_job_id are not in
                # the GPU queue.
                # Handle jobs that aren't patching or peeling.
                elsif r["Patch"].zero? \
                      and Time.now - Time.parse(r["LastChecked"]) > 30
                    # Just check the output of the setup stdout file.
                    setup_stdout = Dir.glob("#{path}/RTS-setup*.out").max_by { |l| File.mtime(l) }
                    if not setup_stdout
                        new_status = "failed"
                    else
                        success = File.read(setup_stdout).match?(/gator .* finished successfully./)
                        new_status = success ? "succeeded" : "failed"
                    end
                elsif Time.now - Time.parse(r["LastChecked"]) > 30
                    new_status, final = check_rts_status(path: path)
                    update_db(db: db, table_name: "rts", obsid: obsid, path: path, stdout: final)
                end
                # Check the stdout file of the RTS run. If it has certain
                # messages, it has failed, and we can kill it early.
                if jobs_in_queue.include? rts_job_id and (status == "rts running" or new_status == "rts running")
                    stdout_file = Dir.glob("#{path}/RTS-\S+-#{obsid}-#{rts_job_id}.out")
                                     .max_by { |l| File.mtime(l) }
                    next unless stdout_file
                    any_update = true
                    status, final = check_rts_status(path: path, stdout_log: stdout_file)
                    next unless status == "failed"
                    any_update = true
                    puts "#{obsid.to_s.red}: #{last_line}"
                    update_db(db: db, table_name: "rts", obsid: obsid, path: path,
                              status: "failed", stdout: final)
                    delete_gpubox_files(path: "#{ENV["MWA_DIR"].chomp("/")}/data/#{obsid}") \
                        if r["DeleteAfterCal"] == 1
                    `scancel #{rts_job_id}`
                    break
                end
                if new_status
                    if final
                        if new_status == "succeeded"
                            puts "#{obsid.to_s.green}: #{new_status} - #{final}"
                        else
                            puts "#{obsid.to_s.red}: #{new_status} - #{final}"
                        end
                        delete_gpubox_files(path: "#{ENV["MWA_DIR"].chomp("/")}/data/#{obsid}") \
                          if r["DeleteAfterCal"] == 1
                    else
                        puts "#{obsid.to_s.blue}: #{new_status}"
                    end
                    any_update = true
                    update_db(db: db, table_name: "rts", obsid: obsid, path: path,
                              status: new_status)
                end

                # We can't do anything further if we're already at the maximum queue length.
                if len_galaxy_queue >= options[:max_rts_queue_length]
                    if options[:debugging?]
                        puts "Breaking in \"main RTS loop\", because:\n" \
                             "\tlen_galaxy_queue >= options[:max_rts_queue_length]"
                    end
                    break
                end

                if status == "unqueued"
                    # We can calibrate this observation only if it is already downloaded.
                    # Mark observations with failed downloads.
                    if tables.include? "dl" \
                      and download_statuses.include?(obsid) \
                      and download_statuses[obsid] == "failed"
                        any_update = true
                        update_db(db: db, table_name: "rts", obsid: obsid,
                                  status: "failed", stdout: "ASVO download error")
                    end
                    next if tables.include? "dl" and \
                            (not download_statuses.include?(obsid) or download_statuses[obsid] != "downloaded")

                    o = Obsid.new(obsid,
                                  path: path == ' ' ? nil : path,
                                  timestamp: r["Timestamp"] == 1,
                                  low_priority: options[:low_job_priority?],
                                  force_ra: r["ForceRA"],
                                  force_dec: r["ForceDec"])
                    o.rts(setup_mins: 10,
                          cal_mins: r["JobTime"],
                          patch: r["Patch"] == 1,
                          peel: r["Peel"] == 1,
                          patch_number: r["PatchNumber"],
                          peel_number: r["PeelNumber"],
                          rts_module_version: r["RTSVersion"],
                          srclists_module_version: r["SrclistsVersion"],
                          srclist: r["Srclist"],
                          use_existing_flags: options[:use_existing_flags],
                          fhd_flag_antennas: fhd ? fhd[obsid] : nil)
                    if o.type == "LymanA"
                        low_path = "#{o.path}/#{o.timestamp_dir}"
                        high_path = "#{o.path}/" << o.timestamp_dir.split("_").select { |e| e =~ /\d/ }.join("_") << "_high"
                        if o.status != "OK"
                            puts "#{obsid.to_s.red}: #{o.status}"
                            update_db(db: db, table_name: "rts", obsid: obsid, path: "high",
                                      status: o.status, new_path: high_path)
                            update_db(db: db, table_name: "rts", obsid: obsid, path: "low",
                                      status: o.status, new_path: low_path)
                            next
                        else
                            update_db(db: db, table_name: "rts", obsid: obsid, path: "high",
                                      status: "setting up", new_path: high_path,
                                      setup_job_id: o.high_setup_jobid, rts_job_id: o.high_patch_jobid)
                            update_db(db: db, table_name: "rts", obsid: obsid, path: "low",
                                      status: "setting up", new_path: low_path,
                                      setup_job_id: o.low_setup_jobid, rts_job_id: o.low_patch_jobid)
                            len_galaxy_queue += 4
                        end
                        any_update = true
                    elsif o.status != "OK"
                        any_update = true
                        puts "#{obsid.to_s.red}: #{o.status}"
                        update_db(db: db, table_name: "rts", obsid: obsid, path: path, status: o.status)
                        next
                    else
                        any_update = true
                        update_db(db: db, table_name: "rts", obsid: obsid, path: path,
                                  status: "setting up", new_path: "#{o.path}/#{o.timestamp_dir}",
                                  setup_job_id: o.setup_jobid, rts_job_id: o.patch_jobid)
                        len_galaxy_queue += 2
                    end
                    puts "Submitted #{obsid.to_s.blue} as RTS job #{o.setup_jobid.to_s.blue} (type: #{o.type})."
                end
            end
        end

        # Check the status of all obsids - exit if they're all done (success or failure).
        status_message = []
        if tables.include? "dl"
            num_not_downloaded = 0
            db.execute("SELECT * FROM dl") do |r|
                num_not_downloaded += 1 unless %w[downloaded failed].include? r["Status"]
            end

            if not tables.include? "rts" and num_not_downloaded.zero?
                puts "\nJobs done.".green
                break
            end

            status_message.push("Number of obsids not yet downloaded: #{num_not_downloaded.to_s.yellow}")
        end
        if tables.include? "rts"
            num_not_calibrated = 0
            db.execute("SELECT * FROM rts") do |r|
                num_not_calibrated += 1 \
                  unless ["succeeded", "failed", "no data", "???", "No gpubox files"].include? r["Status"]
            end

            if not tables.include? "dl" and num_not_calibrated.zero?
                puts "\nJobs done.".green
                break
            elsif tables.include? "dl" and num_not_downloaded.zero? and num_not_calibrated.zero?
                puts "\nJobs done.".green
                break
            end

            status_message.push("Number of obsids not yet calibrated: #{num_not_calibrated.to_s.yellow}")
        end

        if tables.include? "dl"
            status_message.push("ASVO  jobs #{"processing".blue}: #{num_asvo_processing.to_s.yellow}, #{"queued".blue}: #{num_asvo_queued.to_s.yellow}")
            status_message.push("copyq jobs #{"running".blue}: #{len_zeus_queue.to_s.yellow}")
        end
        status_message.push("gpuq  jobs #{"running".blue}: #{len_galaxy_queue.to_s.yellow}") \
          if tables.include? "rts"

        status_message.push("#{Time.now.to_s.blue}: Sleeping...\n\n")
        puts status_message.join("\n") if any_update
        sleep options[:sleep_time]
    end
rescue SQLite3::Exception => e
    puts "#{e.class}: #{e}"
ensure
    db&.close
end
