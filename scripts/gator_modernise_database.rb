#!/usr/bin/env ruby

# This script converts any old sqlite database to the modern form used by gator.
# Necessary as the structure of the tables changes over time.

require "sqlite3"
require "fileutils"
require "optparse"
$LOAD_PATH.unshift "#{File.dirname(__FILE__)}/.."
require "gator"
require "db_operations"

def get_row_from_table(database, table:)
    database.execute("SELECT * FROM #{table}").first
end

def add_column(database, table:, column_name:, type:, default_value: nil)
    puts "Adding column #{column_name} to \"#{table}\" table..."
    database.execute "ALTER TABLE #{table} " \
                     "ADD COLUMN #{column_name} #{type}"
    if default_value and type == "TEXT"
        database.execute("UPDATE #{table} " \
                         "SET #{column_name} = '#{default_value}'")
    else
        database.execute("UPDATE #{table} " \
                         "SET #{column_name} = #{default_value}")
    end
end

def add_column_unless_present(database, table:, column_name:, type:, default_value:)
    row = get_row_from_table(database, table: table)
    add_column(database, table: table, column_name: column_name,
               type: type, default_value: default_value) \
        unless row.include? column_name
end

OptionParser.new do |opts|
    opts.banner = "Usage: #{$PROGRAM_NAME} [options] SQLITE1 SQLITE2 ..."

    opts.on("-h", "--help", "Display this message.") do
        puts opts
        exit
    end
end.parse!

abort("No databases supplied!") if ARGV.empty?

ARGV.each do |f|
    puts "Working on file: #{f}"
    # Backup!
    FileUtils.cp(f, "#{f}.backup")

    db = SQLite3::Database.open f
    db.results_as_hash = true
    tables = tables_in_db(db: db)

    # "downloads" was used for downloads and RTS jobs a long time ago.
    if tables.include? "downloads"
        if db.execute("SELECT * FROM downloads").first.include? "RTSJobID"
            puts "Converting old RTS table (downloads -> rts)..."
            db.execute "ALTER TABLE downloads " \
                       "RENAME TO rts"
        else
            puts "Converting old downloads table (downloads -> dl)..."
            db.execute "ALTER TABLE downloads " \
                       "RENAME TO dl"
        end
    end

    tables = tables_in_db(db: db)
    if tables.include? "dl"
        [["JobTime", "INTEGER", 180],
         ["ConversionOptions", "TEXT", ""]].each do |a|
            if a.length == 2
                add_column_unless_present(db, table: "dl", column_name: a[0], type: a[1])
            elsif a.length == 3
                add_column_unless_present(db, table: "dl", column_name: a[0],
                                          type: a[1], default_value: a[2])
            end
        end
    end
    if tables.include? "rts"
        [["Timestamp", "INTEGER", 1],
         ["Patch", "INTEGER", 1],
         ["Peel", "INTEGER", 1],
         ["PatchNumber", "INTEGER", 1000],
         ["PeelNumber", "INTEGER", 1000],
         ["JobTime", "INTEGER", 40],
         ["RTSVersion", "TEXT", "default"],
         ["SrclistsVersion", "TEXT", "default"],
         ["Srclist", "TEXT", "srclist_pumav3_EoR0aegean_EoR1pietro+ForA.txt"],
         ["DeleteAfterCal", "INTEGER", 0],
         ["ForceRA", "REAL", -9999],
         ["ForceDec", "REAL", -9999]].each do |a|
            if a.length == 2
                add_column_unless_present(db, table: "rts", column_name: a[0], type: a[1])
            else
                add_column_unless_present(db, table: "rts", column_name: a[0],
                                          type: a[1], default_value: a[2])
            end
        end
    end
    puts "Finished working on #{f}"
end
