#!/usr/bin/env python

# Convert FHD Antenna flags to RTS tile flags.
# Metafits files can be obtained with the following:
# wget http://mwa-metadata01.pawsey.org.au/metadata/fits/?obs_id=${obsid} -O ${obsid}.metafits

# Python 2 and 3 compatibility
from __future__ import print_function, division
from future.utils import raise_with_traceback

import argparse
from astropy.io import fits


def fhd2rts(metafits, fhd_flags):
    hdul = fits.open(metafits)

    # `pairs` relates the metafits' 'Antenna' column to 'Input'. The RTS takes
    # 'Input' // 2 as the tile to be flagged.
    pairs = dict(x for x in zip(hdul[1].data["Antenna"], hdul[1].data["Input"]))
    rts_flags = []
    for f in fhd_flags:
        rts_flags.append(str(pairs[int(f)] // 2))
    return rts_flags


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--metafits", type=str, required=True,
                        help="Path to the metafits file to convert the tile numbers.")
    parser.add_argument("fhd_flags", nargs='*', help="FHD 'antenna numbers' to be converted to RTS 'tile numbers'.")
    args = parser.parse_args()

    if not args.fhd_flags:
        raise_with_traceback(ValueError("No FHD flags specified!"))

    print("\n".join(fhd2rts(args.metafits, args.fhd_flags)))
