#!/usr/bin/env ruby

require "sqlite3"
require "optparse"
$LOAD_PATH.unshift "#{File.dirname(__FILE__)}/.."
require "gator"
require "common_cli_args"
require "db_operations"

options = parse_table_cli_args
abort("No database specified! Use --db") unless options[:db]
abort("Database to be modified doesn't exist!") unless File.exist?(options[:db])

# Update the "dl" table.
# Old tables that gator used were always called "downloads". Now use "dl" for downloads.
table_name = "dl"

db = SQLite3::Database.open options[:db]
db.results_as_hash = true
begin
    # The download database rows only really have the variable "JobTime".
    # Compare :download_job_time with the default
    if options[:download_job_time] != 10
        db.execute("SELECT * FROM #{table_name}").each do |row|
            update_db(db: db, table_name: table_name, obsid: o.to_i, job_time: options[:download_job_time])
            puts "Updated download row: #{o}"
        end
    end

    # Update the "rts" table.
    table_name = "rts"

    db.execute "INSERT INTO #{table_name} VALUES(null, #{o}, '#{path}', 'unqueued', '#{Time.now}', 0, 0, ' ', #{options[:timestamp] ? 1 : 0}, #{options[:patch?] ? 1 : 0}, #{options[:peel?] ? 1 : 0}, #{options[:peel_number]})"
    puts "Updated RTS row: #{o}"
rescue SQLite3::Exception => e
    puts "#{e.class}: #{e}"
ensure
    db&.close
end
