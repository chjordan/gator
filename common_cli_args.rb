require "optparse"

def parse_table_cli_args
    options = {}
    OptionParser.new do |opts|
        opts.banner = "Usage: #{$0} [options] <--db /path/to/database/file> " \
          "<file1/observation1> <file2/observation2> ..." \
          "\nAdd obsids to a database for downloading and/or RTS calibration."
        opts.on("-h", "--help", "Display this message.") do
            puts opts
            exit
        end

        options[:db] = nil
        opts.on("--db DATABASE",
                "Specify the database to be used. " \
                  "No default.") \
          { |o| options[:db] = o }

        options[:download_only?] = false
        opts.on("-d", "--download-only",
                "Do not queue specified observations for RTS calibration. " \
                  "Default: #{options[:download_only?]}") \
          { options[:download_only?] = true }

        options[:conversion_options] = nil
        opts.on("--conversion-options STRING",
                "Specify options for an ASVO cotter job. If unused, then " \
                  "gator will simply download the visibilities (default).") \
          { |o| options[:conversion_options] = o }

        options[:calibrate_only?] = false
        opts.on("-c", "--calibrate-only",
                "Do not queue specified observations for downloading. " \
                  "Default: #{options[:calibrate_only?]}") \
          { options[:calibrate_only?] = true }

        options[:patch?] = true
        opts.on("--[no-]patch",
                "Run the patch step. " \
                  "Default: #{options[:patch?]}") \
          { |o| options[:patch?] = o }

        options[:peel?] = true
        opts.on("--[no-]peel",
                "Run the peel step. " \
                  "Default: #{options[:peel?]}") \
          { |o| options[:peel?] = o }

        options[:delete_after_calibration?] = false
        opts.on("--[no-]delete",
                "Delete gpubox files after RTS calibration (even if the RTS fails!). " \
                  "Default: #{options[:delete_after_calibration?]}") \
          { |o| options[:delete_after_calibration?] = o }

        options[:timestamp?] = true
        opts.on("--[no-]timestamp",
                "Isolate RTS calibration runs within timestamp directories. " \
                  "Default: #{options[:timestamp?]}") \
          { |o| options[:timestamp?] = o }

        # options[:rts_gpu] = "#{ENV["RTS_ROOT"]}/bin/rts_gpu"
        # opts.on("--rts PATH",
        #         "Specify the path to the rts_gpu executable to be used. Default: #{options[:rts_gpu]}") \
        #   { |o| options[:rts_gpu] = o }

        options[:patch_number] = 1000
        opts.on("--patch-number NUM",
                "Use this many sources in the patch step of the RTS. " \
                  "Default: #{options[:patch_number]}") \
          { |o| options[:patch_number] = o.to_i }

        options[:peel_number] = 1000
        opts.on("--peel-number NUM",
                "Peel this many sources with the RTS. " \
                  "Default: #{options[:peel_number]}") \
          { |o| options[:peel_number] = o.to_i }

        options[:download_job_time] = 180
        opts.on("--dl-job-time NUM",
                "Allow the download jobs to run for this many minutes. " \
                  "Default: #{options[:download_job_time]}") \
          { |o| options[:download_job_time] = o.to_i }

        options[:rts_job_time] = 60
        opts.on("--rts-job-time NUM",
                "Allow the RTS jobs to run for this many minutes. " \
                  "Default: #{options[:rts_job_time]}") \
          { |o| options[:rts_job_time] = o.to_i }

        options[:rts_version] = "mkl"
        opts.on("--rts-version STRING",
                "Specify the module version to use for the RTS e.g. master " \
                  "Default: #{options[:rts_version]}") \
          { |o| options[:rts_version] = o }

        options[:srclists_version] = "default"
        opts.on("--srclists-version STRING",
                "Specify the module version to use for srclists e.g. master " \
                  "Default: #{options[:srclists_version]}") \
          { |o| options[:srclists_version] = o }

        options[:srclist] = "srclist_pumav3_EoR0aegean_EoR1pietro+ForA.txt"
        opts.on("-s", "--srclist STRING",
                "Specify the source list to be used. Default: #{options[:srclist]}") \
          { |o| options[:srclist] = o }

        options[:force_ra] = nil
        opts.on("--force-ra RA",
                "Specify the RA to be used in calibration (decimal value of hour angle, e.g. 4.0)." \
                " This ignores the RA value in metadata.") \
          { |o| options[:force_ra] = o }

        options[:force_dec] = nil
        opts.on("--force-dec ",
                "Specify the Dec to be used in calibration (decimal value, e.g. -30.0)." \
                " This ignores the Dec value in metadata.") \
          { |o| options[:force_dec] = o }

        options[:coarse_bands] = "all"
        opts.on("--coarse-bands ARRAY", "The coarse band channels to calibrate. Specify like \"1,2,3\". Default: all") \
        { |o| options[:coarse_bands] = o.split(",").map { |x| x.to_i }}
    end.parse!

    # Sanity checks.
    abort("Both download_only and calibrate_only specified!") \
      if options[:download_only?] and options[:calibrate_only?]
    abort("Cannot peel if we are not patching!") if options[:peel?] and not options[:patch?]
    abort("Negative download job time specified!") if options[:download_job_time].negative?
    abort("Negative RTS job time specified!") if options[:rts_job_time].negative?
    abort("Cannot specify --force-ra without --force-dec!") \
        if (options[:force_ra] and not options[:force_dec]) or \
           (options[:force_dec] and not options[:force_ra])

    # Handle "default" values for rts_version and srclists_version.
    rts_versions = Dir.glob("/group/mwa/software/modulefiles/RTS/*")
                      .map { |f| File.basename(f) }
    default_rts_version = File.read("/group/mwa/software/modulefiles/RTS/.version")
                              .match(/ModulesVersion "(\S+)"/)[1]
    options[:rts_version] = default_rts_version if options[:rts_version] == "default"
    # Check if the specified module version actually exists.
    abort("RTS module #{options[:rts_version]} doesn't exist!") \
        unless rts_versions.include? options[:rts_version]

    srclists_versions = Dir.glob("/group/mwa/software/modulefiles/srclists/*")
                           .map { |f| File.basename(f) }
    default_srclists_version = File.read("/group/mwa/software/modulefiles/srclists/.version")
                                   .match(/ModulesVersion "(\S+)"/)[1]
    options[:srclists_version] = default_srclists_version if options[:srclists_version] == "default"
    abort("srclists module #{options[:srclists_version]} doesn't exist!") \
        unless srclists_versions.include? options[:srclists_version]

    srclists_available = Dir.glob("/group/mwa/software/srclists/#{options[:srclists_version]}/*")
                            .map { |f| File.basename(f) }
    # Use the full path if the specified source list is one of Jack's.
    options[:srclist] = "/group/mwa/software/srclists/#{options[:srclists_version]}/#{options[:srclist]}" \
        if srclists_available.include? options[:srclist]
    abort("Source list #{options[:srclist]} doesn't exist!") \
        unless File.exist? options[:srclist]

    options
end
